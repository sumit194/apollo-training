import { ApolloServer, makeExecutableSchema } from 'apollo-server-express';
import express from 'express';
import http from 'http';
import config from './config/configuration';
import { Trainee, User } from './services';
import schema from '.';

const app = express();

const server = new ApolloServer({
  schema: makeExecutableSchema(schema),
  dataSources: () => {
    return {
      userAPI: new User(),
      traineeAPI: new Trainee(),
    };
  },
  context: ({ req, connection }) => {
    if (connection) {
      return connection.context;
    }
    return { authorization: req.headers && req.headers.authorization };
  }
});

server.applyMiddleware({ app });

const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);

httpServer.listen(config.port, () => {
  console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`);
});
