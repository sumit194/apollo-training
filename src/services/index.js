import User from './user/user';
import Trainee from './trainee/trainee';

export {
  User,
  Trainee,
};
