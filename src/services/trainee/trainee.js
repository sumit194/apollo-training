import { RESTDataSource } from 'apollo-datasource-rest';
import config from '../../config/configuration';

class Trainee extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = config.serviceURL;
  }

  willSendRequest(request) {
    request.headers.set('Authorization', this.context.authorization);
  }

  async getTrainee() {
    return this.get(`/trainee`);
  }

  async addTrainee(name, email, password) {
    return this.post(`/trainee`, { name, email, password });
  }

  async updateTrainee(id, name, email, password) {
    return this.put(`/trainee`, { id, name, email, password });
  }

  async deleteTrainee(id) {
    return this.delete(`/trainee/${id}`);
  }
}

export default Trainee;
