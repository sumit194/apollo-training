import { RESTDataSource } from 'apollo-datasource-rest';
import config from '../../config/configuration';

class User extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = config.serviceURL;
  }

  willSendRequest(request) {
    request.headers.set('Authorization', this.context.authorization);
  }

  async getMe() {
    return this.get(`/user/me`);
  }

  async login(email, password) {
    return this.post(`/user/login`, { email, password });
  }
}

export default User;
