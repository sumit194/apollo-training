import { PubSub } from 'apollo-server-express';

const pubsub = new PubSub();

const USER_LOGIN = 'USER_LOGIN';
const TRAINEE_ADDED = 'TRAINEE_ADDED';
const TRAINEE_UPDATED = 'TRAINEE_UPDATED';
const TRAINEE_DELETED = 'TRAINEE_DELETED';

export default pubsub;
export {
  USER_LOGIN,
  TRAINEE_ADDED,
  TRAINEE_UPDATED,
  TRAINEE_DELETED,
};
