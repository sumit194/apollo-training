const Query = {
  me: async (parent, args, { dataSources }) => {
    return await dataSources.userAPI.getMe();
  },
};

export default Query;
