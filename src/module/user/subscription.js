import pubsub, { USER_LOGIN } from '../../subscription';

const Subscription = {
  userLoggedIn: {
    subscribe: () => pubsub.asyncIterator([USER_LOGIN]),
  },
};

export default Subscription;
