import pubsub, { USER_LOGIN } from "../../subscription";

const Mutation = {
  login: async (parent, { email, password }, { dataSources }) => {
    const response = await dataSources.userAPI.login(email, password);
    pubsub.publish(USER_LOGIN, { userLoggedIn: response });
    return response;
  },
};

export default Mutation;
