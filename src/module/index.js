import { Query as UserQuery, Mutation as UserMutation, Subscription as UserSubscription } from './user';
import { Query as TraineeQuery, Mutation as TraineeMutation, Subscription as TraineeSubscription } from './trainee';

export default {
  Query: {
    ...UserQuery,
    ...TraineeQuery,
  },
  Mutation: {
    ...TraineeMutation,
    ...UserMutation,
  },
  Subscription: {
    ...UserSubscription,
    ...TraineeSubscription,
  },
};
