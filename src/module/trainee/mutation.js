import pubsub, { TRAINEE_ADDED, TRAINEE_DELETED, TRAINEE_UPDATED } from '../../subscription';

const Mutation = {
  addTrainee: async (parent, { name, email, password }, { dataSources }) => {
    try {
      const response = await dataSources.traineeAPI.addTrainee(name, email, password);
      pubsub.publish(TRAINEE_ADDED, { traineeAdded: response });
      return response;
    } catch(err) {
      throw new Error('Error');
    }
  },
  updateTrainee: async (parent, { id, name, email, password }, { dataSources }) => {
    try {
      const response = await dataSources.traineeAPI.updateTrainee(id, name, email, password);
      pubsub.publish(TRAINEE_UPDATED, { traineeUpdated: response });
      return response;
    } catch(err) {
      throw new Error('Error');
    }
  },
  deleteTrainee: async (parent, { id }, { dataSources }) => {
    try {
      const response = await dataSources.traineeAPI.deleteTrainee();
      pubsub.publish(TRAINEE_DELETED, { traineeDeleted: response });
      return response;
    } catch(err) {
      throw new Error('Error');
    }
  },
};

export default Mutation;
