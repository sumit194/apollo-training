const Query = {
  getTrainee: async (parent, args, { dataSources }) => {
    return await dataSources.traineeAPI.getTrainee();
  },
};

export default Query;
