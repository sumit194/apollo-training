import pubsub, { TRAINEE_ADDED, TRAINEE_DELETED, TRAINEE_UPDATED } from '../../subscription';

const Subscription = {
  traineeAdded: {
    subscribe: () => pubsub.asyncIterator([TRAINEE_ADDED]),
  },
  traineeUpdated: {
    subscribe: () => pubsub.asyncIterator([TRAINEE_UPDATED]),
  },
  traineeDeleted: {
    subscribe: () => pubsub.asyncIterator([TRAINEE_DELETED]),
  },
};

export default Subscription;
